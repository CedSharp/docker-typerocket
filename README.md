# TypeRocket in Docker

The goal of this project is to create a TypeRocket-ready environment for
quiclkly developing.

## Configuration

### Permission

Nginx and php-fpm usually have their own user for running things, and it
makes local development a pain.

This project expects you to provide your UID (run `id -u`) and your username
(run `id -un`). Copy `.env.example` to `.env` and add your user info.

```
UID=1000
USER=johndoe
```

With this information, nginx and php-fpm will use your user, and there should
be no more file permission conflicts.

*NOTE*: If you change those values after running `docker compose up -d`, you
will have to rebuild the containers. You can use
`docker compose up -d --build php web` for that.

### Nginx configuration

You can edit `./nginx.conf` to your liking. The primary intention is to
change the server name so you can use whatever domain name you want.

Make sure the line `include /etc/nginx/wordpress.conf;` is present in your
server block otherwise all of the wordpress urls won't work.

## New Project

*NOTE*: For this to work, the `./www` must be empty!

Simply run `docker compose up -d`. It will start an nginx, mariadb, php-fpm
environment in which TypeRocket will be installed in `./www` and the
`root:install` command will be executed.

## Existing Project

If the `wp-config.php` file is not found in `./www` then TypeRocket's
`root:install` will automatically be called. *NOTE*: This will fail if
`./www/wordpress` contains wordpress files! Make sure it's empty or only
contains the TypeRocket-related files (ex: `./www/wordpress/assets`).

If `wp-config.php` already exists, nothing happens and the website is assummed
to be properly configured.
