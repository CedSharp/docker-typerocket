#! /bin/sh

until echo '\q' | mysql -h db -u wp_user -pwp_secret wp_db; do
  >&2 echo 'Waiting for DB...'
  sleep 1;
done
echo 'Connected to DB!'

cd /var/www/html
if [ ! -d /var/www/html/app ]; then
  echo 'Installing TypeRocket'
  composer create-project --prefer-dist typerocket/typerocket .
elif [ ! -d /var/www/html/vendor ]; then
  composer install
fi

if [ ! -f /var/www/html/wp-config.php ]; then
  php /var/www/html/galaxy root:install wp_db wp_user wp_secret
  sed -i "s/localhost/db/" /var/www/html/wp-config.php
fi

chown -R $UID:$UID /var/www/html
find /var/www/html/wordpress -type f -exec chmod 644 {} \;
find /var/www/html/wordpress -type d -exec chmod 755 {} \;

# Continue normal execution
docker-php-entrypoint $@
