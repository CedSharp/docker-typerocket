FROM php:8.1.1-fpm-alpine
LABEL maintainer="Cedrik Dubois"

ARG UID=1000
ARG USER=username

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN apk add --no-cache gd libpng-dev libzip-dev mysql-client unzip zip zlib-dev;
RUN docker-php-ext-install gd mysqli zip; \
    docker-php-ext-enable gd mysqli zip;

RUN chmod uga+x /usr/local/bin/install-php-extensions && sync && \
    install-php-extensions imagick;

RUN php -r "copy('https://getcomposer.org/installer', 'installer');"; \
    php installer --install-dir=/usr/bin --filename=composer; \
    php -r "unlink('installer');";

ADD ./php-entry.sh /usr/bin/php-entry.sh
RUN chmod +x /usr/bin/php-entry.sh;

RUN addgroup -g $UID -S $USER; adduser -u $UID -S $USER -G $USER;
RUN sed -i "s/www-data/$USER/" /usr/local/etc/php-fpm.d/www.conf; \
    chown -R $USER:$USER /var/www/html;

ENTRYPOINT ["php-entry.sh"]
CMD ["php-fpm"]
