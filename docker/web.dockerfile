FROM nginx:1.21.4-alpine
LABEL maintainer="Cedrik Dubois"

ARG UID=1000
ARG USER=username

COPY web.base.conf /etc/nginx/nginx.conf
COPY web.wordpress.conf /etc/nginx/wordpress.conf

USER root
RUN addgroup -g $UID -S $USER; adduser -u $UID -S $USER -G $USER; \
    sed -i "s/\[USER\]/$USER/" /etc/nginx/nginx.conf; \
    sed -i "s/\[GROUP\]/$USER/" /etc/nginx/nginx.conf;
